module.exports = () => {
  $.gulp.task('criticalCss', function () {
    if ($.config.criticalCss) {
      return $.gulp.src($.config.destPath + '/html/**/*.html')
        .pipe($.critical({
          inline: true,
          base: 'dist/html/',
          minify: true,
          css: ['dist/static/css/main.css'],
          dimensions: [{
            height: 480,
            width: 320
          }, {
            height: 1600,
            width: 1300
          }]
        }))
        .on('error', function (err) {
          $.gulpPlugin.util.log($.gulpPlugin.util.colors.red(err.message));
        })
        .pipe($.gulp.dest($.config.destPath + '/html/'));
    }

    return $.gulp.src($.config.destPath + '/html/**/*.html')
      .pipe($.gulp.dest($.config.destPath + '/html/'));
  });
};