'use strict';

(function () {
  const App = {
    openMenu() {
      let $body = document.body;
      let $menu = document.querySelector('.js-menu');

      if (!$menu) {
        return false
      }

      let $btnOpen = $menu.querySelector('.js-menu-open');
      let $btnClose = $menu.querySelector('.js-menu-close');

      $btnOpen.addEventListener('click', function () {
        $body.classList.add('nav-opened');
      });

      $btnClose.addEventListener('click', function () {
        $body.classList.remove('nav-opened');
      });
    },
    init() {
      this.openMenu();
      $('.js-dropdown-box').each(function () {
        $(this).dropdown({
          prefix: $(this).data('prefix')
        });
      });
    }
  };

  window.addEventListener('load', () => {
    window.svg4everybody();
    App.init();

    document.documentElement.addEventListener('touchstart', function (event) {
      if (event.touches.length > 1) {
        event.preventDefault();
      }
    }, false);
  });
})();